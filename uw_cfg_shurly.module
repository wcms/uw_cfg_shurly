<?php

/**
 * @file
 * Code for the uWaterloo ShURLy configuration feature.
 */

include_once 'uw_cfg_shurly.features.inc';

/**
 * Implements hook_boot().
 *
 * Needed so this module loads on boot so that the shurly_redirect_after hook works.
 */
function uw_cfg_shurly_boot() {
  return;
}

/**
 * Implements hook_shurly_redirect_after().
 */
function uw_cfg_shurly_shurly_redirect_after($row) {
  // $row is an object with ->rid and ->destination.
  // This function adds tracking for every redirect, tracking the time used and the IP address.
  db_insert('shurly_uw_tracking')
    ->fields(array(
      'rid' => $row->rid,
      'used' => time(),
      'ip' => ip_address(),
    ))->execute();
}

/**
 * Implements hook_views_data().
 */
function uw_cfg_shurly_views_data() {
  // Define UI prefix.
  $data['shurly_uw_tracking']['table']['group'] = t('ShURLy tracking');

  // Define as base table.
  $data['shurly_uw_tracking']['table']['base'] = array(
    // This is the identifier field for the view.
    'field' => 'tid',
    'title' => t('ShURLy tracking'),
    'help' => t('ShURLy tracking contains individual data for redirect times and source IPs.'),
    'weight' => -10,
  );

  // This table references the 'shurly' table.
  $data['shurly_uw_tracking']['table']['join'] = array(
    'shurly' => array(
      'left_field' => 'rid',
      'field' => 'rid',
    ),
  );

  // Now let views know about all the fields.
  $data['shurly_uw_tracking']['rid'] = array(
    'title' => t('Redirect ID'),
    'help' => t('Redirect ID'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'shurly',
      'base field' => 'rid',
      'handler' => 'views_handler_relationship',
      'title' => t('Cross reference to the actual ShURLy redirects'),
      'help' => t('Needed to get URL information'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['shurly_uw_tracking']['ip'] = array(
    'title' => t('IP used'),
    'help' => t('The IP address using this specific redirect.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['shurly_uw_tracking']['used'] = array(
    'title' => t('Date used'),
    'help' => t('The date this specific redirect was used.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['shurly']['link_detail'] = array(
    'field' => array(
      'title' => t('URL usage detail'),
      'help' => t('Link to URL usage detail'),
      'handler' => 'shurly_uw_tracking_handler_field_link_detail',
      'parent' => 'views_handler_field',
    ),
  );

  $data['shurly']['rid'] = array(
    'title' => t('Redirect ID'),
    'help' => t('Redirect ID'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_pre_render().
 */
function uw_cfg_shurly_views_pre_render(&$view) {
  if ($view->name == 'shurly_specific_url' && $view->current_display == 'page') {
    // Hide the exposed filters when there are no results.
    if (empty($view->result) && empty($view->exposed_input)) {
      $view->exposed_widgets = NULL;
    }
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_cfg_shurly_views_default_views_alter(&$views) {
  // Alter only the 'shurly_my_urls' view.
  if (array_key_exists('shurly_my_urls', $views)) {
    // This is the "easy way" - we have just copied the entire modified view here, then at the end we overwrite the view from the Shurly module.
    $view = new view();
    $view->name = 'shurly_my_urls';
    $view->description = 'My short URLs';
    $view->tag = 'shurly';
    $view->base_table = 'shurly';
    $view->human_name = '';
    $view->core = 0;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Defaults */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->display->display_options['title'] = 'My short URLs';
    $handler->display->display_options['use_ajax'] = TRUE;
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['access']['perm'] = 'View own URL stats';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'none';
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
      'count' => 'count',
      'destination_1' => 'destination_1',
      'destination' => 'destination',
      'source' => 'source',
      'nothing' => 'nothing',
      'created' => 'created',
      'last_used' => 'last_used',
      'link_detail' => 'link_delete',
      'link_delete' => 'link_delete',
    );
    $handler->display->display_options['style_options']['default'] = 'created';
    $handler->display->display_options['style_options']['info'] = array(
      'count' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'destination_1' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'destination' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'source' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'nothing' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'created' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'last_used' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'link_detail' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'link_delete' => array(
        'align' => '',
        'separator' => ' &nbsp; ',
        'empty_column' => 0,
      ),
    );
    /* Field: Shurly: Clicks */
    $handler->display->display_options['fields']['count']['id'] = 'count';
    $handler->display->display_options['fields']['count']['table'] = 'shurly';
    $handler->display->display_options['fields']['count']['field'] = 'count';
    /* Field: Shurly: Long URL */
    $handler->display->display_options['fields']['destination_1']['id'] = 'destination_1';
    $handler->display->display_options['fields']['destination_1']['table'] = 'shurly';
    $handler->display->display_options['fields']['destination_1']['field'] = 'destination';
    $handler->display->display_options['fields']['destination_1']['label'] = 'Long URL clipped';
    $handler->display->display_options['fields']['destination_1']['exclude'] = TRUE;
    $handler->display->display_options['fields']['destination_1']['alter']['max_length'] = '60';
    $handler->display->display_options['fields']['destination_1']['alter']['trim'] = TRUE;
    /* Field: Shurly: Long URL */
    $handler->display->display_options['fields']['destination']['id'] = 'destination';
    $handler->display->display_options['fields']['destination']['table'] = 'shurly';
    $handler->display->display_options['fields']['destination']['field'] = 'destination';
    $handler->display->display_options['fields']['destination']['exclude'] = TRUE;
    $handler->display->display_options['fields']['destination']['alter']['text'] = '<a href="[destination]" title="[destination]" target="_blank">[destination_1]</a>';
    $handler->display->display_options['fields']['destination']['alter']['path'] = '[destination]';
    $handler->display->display_options['fields']['destination']['alter']['alt'] = '[destination]';
    $handler->display->display_options['fields']['destination']['alter']['target'] = '_blank';
    /* Field: Shurly: Short URL */
    $handler->display->display_options['fields']['source']['id'] = 'source';
    $handler->display->display_options['fields']['source']['table'] = 'shurly';
    $handler->display->display_options['fields']['source']['field'] = 'source';
    $handler->display->display_options['fields']['source']['exclude'] = TRUE;
    $handler->display->display_options['fields']['source']['alter']['path'] = '[source]';
    $handler->display->display_options['fields']['source']['alter']['target'] = '_blank';
    $handler->display->display_options['fields']['source']['longshort'] = '0';
    $handler->display->display_options['fields']['source']['link'] = 0;
    /* Field: Global: Custom text */
    $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
    $handler->display->display_options['fields']['nothing']['table'] = 'views';
    $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
    $handler->display->display_options['fields']['nothing']['label'] = 'Links';
    $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="shurly-long"><a href="[source]" title="[destination]" target="_blank">[destination_1]</a></div>
    <div class="shurly-short">[source]</div>';
    /* Field: Shurly: Created date */
    $handler->display->display_options['fields']['created']['id'] = 'created';
    $handler->display->display_options['fields']['created']['table'] = 'shurly';
    $handler->display->display_options['fields']['created']['field'] = 'created';
    $handler->display->display_options['fields']['created']['label'] = 'Date';
    $handler->display->display_options['fields']['created']['date_format'] = 'custom';
    $handler->display->display_options['fields']['created']['custom_date_format'] = 'M j';
    /* Field: Shurly: Last used date */
    $handler->display->display_options['fields']['last_used']['id'] = 'last_used';
    $handler->display->display_options['fields']['last_used']['table'] = 'shurly';
    $handler->display->display_options['fields']['last_used']['field'] = 'last_used';
    $handler->display->display_options['fields']['last_used']['label'] = 'Last used';
    $handler->display->display_options['fields']['last_used']['empty'] = 'Never';
    $handler->display->display_options['fields']['last_used']['date_format'] = 'time ago';
    $handler->display->display_options['fields']['last_used']['custom_date_format'] = '1';
    /* Field: ShURLy tracking: URL usage detail */
    $handler->display->display_options['fields']['link_detail']['id'] = 'link_detail';
    $handler->display->display_options['fields']['link_detail']['table'] = 'shurly';
    $handler->display->display_options['fields']['link_detail']['field'] = 'link_detail';
    $handler->display->display_options['fields']['link_detail']['label'] = '';
    $handler->display->display_options['fields']['link_detail']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['link_detail']['element_default_classes'] = FALSE;
    /* Field: Shurly: Delete URL */
    $handler->display->display_options['fields']['link_delete']['id'] = 'link_delete';
    $handler->display->display_options['fields']['link_delete']['table'] = 'shurly';
    $handler->display->display_options['fields']['link_delete']['field'] = 'link_delete';
    $handler->display->display_options['fields']['link_delete']['label'] = 'Actions';
    /* Sort criterion: Shurly: Created date */
    $handler->display->display_options['sorts']['created']['id'] = 'created';
    $handler->display->display_options['sorts']['created']['table'] = 'shurly';
    $handler->display->display_options['sorts']['created']['field'] = 'created';
    $handler->display->display_options['sorts']['created']['order'] = 'DESC';
    /* Filter criterion: Shurly: Current user */
    $handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
    $handler->display->display_options['filters']['uid_current']['table'] = 'shurly';
    $handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';
    $handler->display->display_options['filters']['uid_current']['value'] = '1';
    $handler->display->display_options['filters']['uid_current']['group'] = '0';
    $handler->display->display_options['filters']['uid_current']['expose']['operator'] = FALSE;
    /* Filter criterion: Shurly: URL active */
    $handler->display->display_options['filters']['active']['id'] = 'active';
    $handler->display->display_options['filters']['active']['table'] = 'shurly';
    $handler->display->display_options['filters']['active']['field'] = 'active';
    $handler->display->display_options['filters']['active']['value'] = '1';
    $handler->display->display_options['filters']['active']['group'] = '0';
    $handler->display->display_options['filters']['active']['expose']['operator'] = FALSE;

    /* Display: My URLs */
    $handler = $view->new_display('page', 'My URLs', 'page_1');
    $handler->display->display_options['path'] = 'myurls';
    $handler->display->display_options['menu']['type'] = 'normal';
    $handler->display->display_options['menu']['title'] = 'My URLs';
    $handler->display->display_options['menu']['description'] = 'List, sort, and delete short URLs';
    $handler->display->display_options['menu']['weight'] = '0';

    /* Display: Block */
    $handler = $view->new_display('block', 'Block', 'block_1');
    $handler->display->display_options['block_description'] = 'My short URLs';
    $translatables['shurly_my_urls'] = array(
      t('Defaults'),
      t('My short URLs'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Clicks'),
      t('Long URL clipped'),
      t('Long URL'),
      t('<a href="[destination]" title="[destination]" target="_blank">[destination_1]</a>'),
      t('[destination]'),
      t('Short URL'),
      t('Links'),
      t('<div class="shurly-long"><a href="[source]" title="[destination]" target="_blank">[destination_1]</a></div>
    <div class="shurly-short">[source]</div>'),
      t('Date'),
      t('Last used'),
      t('Never'),
      t('Actions'),
      t('My URLs'),
      t('Block'),
    );

    $views['shurly_my_urls'] = $view;
  }
}

/**
 * Implements hook_form_alter().
 */
function uw_cfg_shurly_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'shurly_create_form') {
    // dsm($form);
    if ($form['long_url']['#default_value'] == 'http://') {
      $form['long_url']['#default_value'] = '';
    }
    $form['long_url']['#attributes']['placeholder'] = 'https://uwaterloo.ca';
    $form['short_url']['#suffix'] = '<p>If the custom URL is left blank, one will be automatically assigned.</p>';
    $form['submit']['#prefix'] = '<p>';
    $form['submit']['#suffix'] = '</p>';
    $form['#validate'][] = 'uw_cfg_shurly_validate';
    unset($form['long_url']['#attributes']['tabindex']);
    unset($form['short_url']['#attributes']['tabindex']);
    unset($form['submit']['#attributes']['tabindex']);
  }
}

/**
 * Form #validate callback.
 */
function uw_cfg_shurly_validate($form, &$form_state) {
  // Allow only (*.)uwaterloo.ca destinations.
  if (!preg_match(',^https?://([a-z0-9-]+\.)*uwaterloo\.ca(/.*)?$,', $form_state['input']['long_url'])) {
    form_set_error('long_url', 'The destination URL must be a university-approved address.');
  }
  // Prevent selected short URLs (a losing battle, to be sure).
  if (!empty($form_state['input']['short_url']) && in_array(strtolower($form_state['input']['short_url']), array(
    'ass',
    'balls',
    'cocksucker',
    'cunt',
    'faggot',
    'fuck',
    'motherfucker',
    'nigger',
    'piss',
    'shit',
    'tits',
  ))) {
    form_set_error('short_url', 'Invalid custom URL.');
  }
}

/**
 * Field handler to present a link to the short URL entry.
 */
class shurly_uw_tracking_handler_field_link_detail extends views_handler_field {

  /**
   *
   */
  public function construct() {
    parent::construct();
    // $this->additional_fields['uid'] = 'uid';
    // $this->additional_fields['active'] = 'active';.
    $this->additional_fields['rid'] = 'rid';
  }

  /**
   *
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  /**
   *
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  /**
   *
   */
  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   *
   */
  public function render($values) {
    // Only allow the user to view the link if they can administer short URLs.
    if (user_access('Administer short URLs')) {
      $text = !empty($this->options['text']) ? $this->options['text'] : t('detail');
      $rid = $values->rid;
      return l($text, "shurly/detail/$rid");
    }
  }

}
