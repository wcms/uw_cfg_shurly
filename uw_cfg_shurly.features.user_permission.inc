<?php

/**
 * @file
 * uw_cfg_shurly.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_shurly_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'Administer short URLs'.
  $permissions['Administer short URLs'] = array(
    'name' => 'Administer short URLs',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'shurly',
  );

  // Exported permission: 'Create short URLs'.
  $permissions['Create short URLs'] = array(
    'name' => 'Create short URLs',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'shurly',
  );

  // Exported permission: 'Delete own URLs'.
  $permissions['Delete own URLs'] = array(
    'name' => 'Delete own URLs',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'shurly',
  );

  // Exported permission: 'Enter custom URLs'.
  $permissions['Enter custom URLs'] = array(
    'name' => 'Enter custom URLs',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'shurly',
  );

  // Exported permission: 'View own URL stats'.
  $permissions['View own URL stats'] = array(
    'name' => 'View own URL stats',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'shurly',
  );

  return $permissions;
}
