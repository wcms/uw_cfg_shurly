<?php

/**
 * @file
 * uw_cfg_shurly.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_cfg_shurly_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'shurly';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'shurly-form' => array(
          'module' => 'shurly',
          'delta' => 'form',
          'region' => 'content',
          'weight' => '1',
        ),
        'views-shurly_my_urls-block_1' => array(
          'module' => 'views',
          'delta' => 'shurly_my_urls-block_1',
          'region' => 'content',
          'weight' => '2',
        ),
        'shurly-bookmarklet' => array(
          'module' => 'shurly',
          'delta' => 'bookmarklet',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['shurly'] = $context;

  return $export;
}
