<?php

/**
 * @file
 * uw_cfg_shurly.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_cfg_shurly_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_short-url-list:admin/build/shurly/.
  $menu_links['menu-site-management_short-url-list:admin/build/shurly/'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/build/shurly/',
    'router_path' => 'admin/build/shurly',
    'link_title' => 'Short URL list',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_short-url-list:admin/build/shurly/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Short URL list');

  return $menu_links;
}
