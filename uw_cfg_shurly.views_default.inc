<?php

/**
 * @file
 * uw_cfg_shurly.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_cfg_shurly_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'shurly_specific_url';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'shurly_uw_tracking';
  $view->human_name = 'Shurly specific URL';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'URL usage detail';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'Administer short URLs';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'rid' => 'rid',
    'source' => 'source',
    'destination' => 'destination',
    'ip' => 'ip',
    'used' => 'used',
  );
  $handler->display->display_options['style_options']['default'] = 'used';
  $handler->display->display_options['style_options']['info'] = array(
    'rid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'source' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'destination' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ip' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'used' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'no results';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<p>The short URL has not been used.</p>';
  $handler->display->display_options['empty']['area']['format'] = 'uw_tf_standard';
  /* Relationship: ShURLy tracking: Cross reference to the actual ShURLy redirects */
  $handler->display->display_options['relationships']['rid']['id'] = 'rid';
  $handler->display->display_options['relationships']['rid']['table'] = 'shurly_uw_tracking';
  $handler->display->display_options['relationships']['rid']['field'] = 'rid';
  /* Field: Shurly: Short URL */
  $handler->display->display_options['fields']['source']['id'] = 'source';
  $handler->display->display_options['fields']['source']['table'] = 'shurly';
  $handler->display->display_options['fields']['source']['field'] = 'source';
  $handler->display->display_options['fields']['source']['relationship'] = 'rid';
  $handler->display->display_options['fields']['source']['label'] = 'Short URL (source)';
  $handler->display->display_options['fields']['source']['longshort'] = '0';
  $handler->display->display_options['fields']['source']['link'] = 0;
  /* Field: Shurly: Long URL */
  $handler->display->display_options['fields']['destination']['id'] = 'destination';
  $handler->display->display_options['fields']['destination']['table'] = 'shurly';
  $handler->display->display_options['fields']['destination']['field'] = 'destination';
  $handler->display->display_options['fields']['destination']['relationship'] = 'rid';
  $handler->display->display_options['fields']['destination']['label'] = 'Long URL (destination)';
  /* Field: ShURLy tracking: IP used */
  $handler->display->display_options['fields']['ip']['id'] = 'ip';
  $handler->display->display_options['fields']['ip']['table'] = 'shurly_uw_tracking';
  $handler->display->display_options['fields']['ip']['field'] = 'ip';
  /* Field: ShURLy tracking: Date used */
  $handler->display->display_options['fields']['used']['id'] = 'used';
  $handler->display->display_options['fields']['used']['table'] = 'shurly_uw_tracking';
  $handler->display->display_options['fields']['used']['field'] = 'used';
  $handler->display->display_options['fields']['used']['date_format'] = 'today time ago';
  $handler->display->display_options['fields']['used']['second_date_format'] = 'long';
  /* Contextual filter: Shurly: Redirect ID */
  $handler->display->display_options['arguments']['rid']['id'] = 'rid';
  $handler->display->display_options['arguments']['rid']['table'] = 'shurly';
  $handler->display->display_options['arguments']['rid']['field'] = 'rid';
  $handler->display->display_options['arguments']['rid']['relationship'] = 'rid';
  $handler->display->display_options['arguments']['rid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['rid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['rid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['rid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['rid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['rid']['limit'] = '0';
  /* Filter criterion: ShURLy tracking: IP used */
  $handler->display->display_options['filters']['ip']['id'] = 'ip';
  $handler->display->display_options['filters']['ip']['table'] = 'shurly_uw_tracking';
  $handler->display->display_options['filters']['ip']['field'] = 'ip';
  $handler->display->display_options['filters']['ip']['operator'] = 'contains';
  $handler->display->display_options['filters']['ip']['group'] = 1;
  $handler->display->display_options['filters']['ip']['exposed'] = TRUE;
  $handler->display->display_options['filters']['ip']['expose']['operator_id'] = 'ip_op';
  $handler->display->display_options['filters']['ip']['expose']['label'] = 'IP used';
  $handler->display->display_options['filters']['ip']['expose']['operator'] = 'ip_op';
  $handler->display->display_options['filters']['ip']['expose']['identifier'] = 'ip';
  $handler->display->display_options['filters']['ip']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    17 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'shurly/detail';
  $export['shurly_specific_url'] = $view;

  return $export;
}
