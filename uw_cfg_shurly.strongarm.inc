<?php

/**
 * @file
 * uw_cfg_shurly.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_shurly_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shurly_counter';
  $strongarm->value = 3269;
  $export['shurly_counter'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shurly_throttle';
  $strongarm->value = array(
    2 => array(
      'rate' => '5',
      'time' => '60',
      'weight' => '0',
    ),
    3 => array(
      'rate' => '15',
      'time' => '60',
      'weight' => '-1',
    ),
  );
  $export['shurly_throttle'] = $strongarm;

  return $export;
}
